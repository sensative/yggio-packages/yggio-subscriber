'use strict';

const _ = require('lodash');
const subscriberApi = require('./subscriber-api');
const bodyParser = require('body-parser').json();

const subscriptionTypes = {
  IOTNODE: 'IOTNODE',
  RULE: 'RULE',
  ALARM: 'ALARM'
};

let messageAddress = ''; // e.g. https://some-service-domain.io
let messageRoute = ''; // e.g. /message-route
let providerDetails = null;

const setMessageRoute = (address, route, app, onMessage) => {
  messageAddress = address || messageAddress;
  messageRoute = route || messageRoute;
  if (app && onMessage) {
    app.post(messageRoute, bodyParser, (req, res) => {
      const node = _.get(req, 'body', {});
      node._id = node.iotnode_id;
      delete node.channel_id;
      delete node.iotnode_id;
      onMessage(node);
      res.status(200).json();
    });
  }
};

const connect = (yggioConfig) => {
  return Promise.resolve()
    .then(() => {
      // login if needed, otherwise pass the token down the line
      return subscriberApi.login(yggioConfig);
    })
    .then(token => {
      // register a provider
      return subscriberApi.registerProvider(token, yggioConfig.provider)
        .then((provider) => {
          providerDetails = {
            clientId: provider['client_id'],
            secret: provider.secret
          };
          // set the stored subscribe function
          storedSubscribe = (userToken, yggioId, subscriptionType) => {
            const type = subscriptionType || subscriptionTypes.IOTNODE;
            return subscriberApi.subscribe(userToken, messageAddress + messageRoute, yggioId, type);
          };
          // and done
          return Promise.resolve();
        });
    })
    .then(() => {
      console.log('Subscriber is connected');
      return Promise.resolve();
    })
    .catch(err => {
      console.error('err registering provider: ', err.message);
      return Promise.reject(err);
    });
};

let storedSubscribe = null;
const subscribe = (yggioId, subscriptionType) => {
  if (!storedSubscribe) {
    return Promise.reject(new Error('subscriber has not been initialized'));
  }
  return storedSubscribe(yggioId, subscriptionType);
};

module.exports = {
  connect,
  setMessageRoute,
  subscribe,
  types: subscriptionTypes,
  getProviderDetails: () => providerDetails
};
