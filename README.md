# This package will be replaced by 'yggio-service-provider'

## yggio-subscriber: listen to the publisher

yggio-subscriber makes it easier to log in to yggio and subscribe to yggio events

### Usage

Initialize (note that mongoose is injected as an argument)
```javascript
// set up the subscriber
const app = require('express')(); // yeah, we need an express app..
const subscriber = require('yggio-subscriber');
const onMessage = message => {
  doStuff(message)
};
// the config is really the information-dense part of this
const yggioConfig = {
  messageRoute: '/yggio-message',
  rulesUrl: 'https://yggio-rules-example-url',
  yggioUrl: 'https://yggio-device-example-url',
  service: {
    username: 'my-new-service@test.com',
    password: 'so-secret-so-secret'
  },
  provider: {
    name: 'MNS',
    info: 'My New Service Consolidated',
    redirect_uris: {
      browser: 'https://login-redirect-example-url',
      app: 'mns://login'
    },
    logoPath: './path/to/logo.png'
  }
};
const address = 'https://example-domain';
// set the subscriber route message route
subscriber.setMessageRoute(address, yggioConfig.messageRoute, app, onMessage);
```
and then login:
```javascript
subscriber.connect(config.yggio)
  .then(() => {
    // the provider details are used by oauth stuffs
    const providerDetails = subscriber.getProviderDetails();
  });
```
and now we can subscribe to iotnode changes published on the value exchange
```javascript
const user = {
  storm_access_token: 'something_special'
};
const iotnodeId = 'asdf1234';
subscriber.subscribe(user.storm_access_token, iotnodeId);
```
and now the onMessage function should be working.

### License
yggio-populate-request is under the MIT license. See LICENSE file.
