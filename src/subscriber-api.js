'use strict';
// This uses only the subset of yggio-api that pertains to channels & providers

const _ = require('lodash');
const delay = require('delay');
const fs = require('fs');
const yggioApi = require('yggio-api');

const redundantLogin = (yggioConfig) => {
  yggioApi.setYggioConfig(yggioConfig);
  const user = yggioConfig.service;
  console.log('redundantLogin user: ', user);
  return yggioApi.register(user)
    .catch(err => {
      console.warn('yggio-subscriber: failed to register, trying to login instead');
      return yggioApi.login(user);
    })
    .then(res => {
      const token = _.get(res, 'token', '');
      console.info('Register/Login token: ', token);
      return Promise.resolve(token);
    })
    .catch(err => {
      console.warn('Register/Login failed: ' + err.message + '. Retrying in 5 seconds');
      return delay(5000).then(() => {
        redundantLogin(yggioConfig);
      });
    });
};

const registerProvider = (token, providerConfig) => {
  return yggioApi.registerProvider(token, {
    // this should just really be the 'provider' object
    'name': providerConfig.name,
    'info': providerConfig.info,
    'redirect_uri': _.values(providerConfig.redirect_uris)
  })
  .then(provider => {
    return yggioApi.setLogo(token, provider, providerConfig.logoPath)
      .then(() => {
        return Promise.resolve(provider);
      });
  });
};

const subscribe = (token, channelUrl, yggioId, subscriptionType) => {
  return yggioApi.subscribe(token, {
    address: channelUrl,
    iotnode: yggioId,
    subscriptionType
  });
};

module.exports = {
  login: redundantLogin,
  registerProvider,
  subscribe
};
